# Josef's fork of dwmblocks

## dwmblocks

Modular status bar for dwm written in c. Here is the original: <https://github.com/torrinfail/dwmblocks>.

## My changes

- dwmblocks-statuscmd patch (dwm needs dwm-statuscmd patch).
- Commands triggered from incoming signals (when click/scroll events occur) are forked and run asynchronously.
- Commands are executed using /bin/bash instead of /bin/sh.
- Removed block icons.
- Delimiter between blocks can be multiple characters.
- A prefix and suffix can be set to have different characters before and after each block.

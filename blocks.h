static const Block blocks[] = {
    // Command                                            Update interval  Update signal
    {"free -h | awk '/^Mem/ { print $3 }' | sed s/i//g",  5,               12},
    {"~/.local/bin/statusbar/volume.sh",                  5,               11},
    {"~/.local/bin/statusbar/timeanddate.sh",             30,              10},
};

static char *delim  = "   ";
static char *prefix = "";
static char *suffix = "";